# APDU规范

- OpenHarmony安全芯片管理框架高安业务APDU规范
    - [前言](1.md)
    - [概述](2.md)
    - [口令认证APDU](3.md)
    - [安全存储APDU](4.md)
