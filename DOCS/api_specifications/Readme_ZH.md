# API规范

- OpenHarmony安全芯片管理框架高安业务API规范
    - [前言](1.md)
    - [概述](2.md)
    - [公共管理API](3.md)
    - [口令认证API](4.md)
    - [安全存储API](5.md)
