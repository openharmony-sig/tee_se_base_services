/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DYN_CLIENT_INC_BASE_H
#define DYN_CLIENT_INC_BASE_H

#include <stdint.h>

#include "se_base_services_defines.h"

#ifdef __cplusplus
extern "C" {
#endif

#define PUBLIC_API __attribute__((visibility("default")))

#define SERVICE_NAME "se_base_services"

#define SERVICE_UUID                                       \
    {                                                      \
        0x80c73372, 0xccd3, 0x4b95,                        \
        {                                                  \
            0x8f, 0xae, 0xd9, 0x23, 0x5a, 0x35, 0x45, 0x5c \
        }                                                  \
    }

ResultCode SendRequestToServer(uint32_t cmd, const uint8_t *data, uint32_t dataLen, uint8_t *reply, uint32_t *replyLen);

#ifdef __cplusplus
}
#endif

#endif // DYN_CLIENT_INC_BASE_H