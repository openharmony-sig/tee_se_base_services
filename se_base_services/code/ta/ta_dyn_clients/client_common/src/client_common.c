/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "client_common.h"

#include "client_base.h"

#include "module_common_ipc_proxy.h"

PUBLIC_API ResultCode SeCommonIsServiceAvailable(ServiceId sid, SeServiceStatus *status)
{
    return SeCommonIsServiceAvailableProxy(SendRequestToServer, sid, status);
}

PUBLIC_API ResultCode SeCommonSetServiceConfiguration(ServiceId sid, uint32_t lock, const SeServiceConfig *config)
{
    return SeCommonSetServiceConfigurationProxy(SendRequestToServer, sid, lock, config);
}

PUBLIC_API ResultCode SeCommonSetBindingKey(uint8_t *initKey, uint32_t initKeyLength, uint32_t initKvn, ServiceId sid)
{
    return SeCommonSetBindingKeyProxy(SendRequestToServer, initKey, initKeyLength, initKvn, sid);
}

PUBLIC_API ResultCode SeCommonDeleteInitKey(uint8_t *initKey, uint32_t initKeyLength, uint32_t initKvn)
{
    return SeCommonDeleteInitKeyProxy(SendRequestToServer, initKey, initKeyLength, initKvn);
}