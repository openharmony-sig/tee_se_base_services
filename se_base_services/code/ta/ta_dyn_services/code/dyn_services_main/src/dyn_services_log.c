/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "dyn_services_log.h"

#include <stddef.h>

#include <tee_log.h>

#include "logger.h"

void TaDebug(const char *message)
{
    if (message == NULL) {
        return;
    }
    SLogTrace("%s", message);
}

void TaInfo(const char *message)
{
    if (message == NULL) {
        return;
    }
    SLogTrace("%s", message);
}

void TaError(const char *message)
{
    if (message == NULL) {
        return;
    }
    SLogError("%s", message);
}

void ServiceLoggerInit(void)
{
    Logger logger = {
        .debug = TaDebug,
        .error = TaError,
        .info = TaInfo,
    };
    SetApduLogger(&logger);

    LOG_INFO("ServiceLoggerInit success");
}