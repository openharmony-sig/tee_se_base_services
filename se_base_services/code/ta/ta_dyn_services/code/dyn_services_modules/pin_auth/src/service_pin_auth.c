/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "service_pin_auth.h"

#include <securec.h>

#include "dyn_services_card_channel.h"
#include "dyn_services_core.h"
#include "logger.h"
#include "pin_auth_ipc_stub.h"
#include "se_base_services_defines.h"

#define PIN_AUTH_TA_UUID                                   \
    {                                                      \
        0xebeed547, 0xcf6a, 0x4592,                        \
        {                                                  \
            0xb0, 0xf1, 0x2a, 0xe4, 0xa8, 0xcd, 0x43, 0xda \
        }                                                  \
    }

#define PIN_AUTH_APPLET_AID                                                                            \
    {                                                                                                  \
        0xF0, 0x00, 0x00, 0x48, 0x4D, 0x53, 0x45, 0x43, 0x70, 0x69, 0x6E, 0x61, 0x75, 0x74, 0x68, 0x00 \
    }

#define PIN_AUTH_READER_NAME "eSE_spi_0"

ResultCode PinAuthCheckPermission(uint32_t sender, uint32_t sid, uint32_t cmd)
{
    const TEE_UUID list[] = {PIN_AUTH_TA_UUID};

    if (CheckClientPermission(sender, list, sizeof(list) / sizeof(TEE_UUID)) != SUCCESS) {
        LOG_ERROR("error for CheckClientPermission, sid = 0x%x, cmd = 0x%x", sid, cmd);
        return INVALID_PERM;
    }
    return SUCCESS;
}

ResultCode PinAuthProcessCommand(uint32_t sender, uint32_t cmd, SharedDataBuffer *sharedData)
{
    if (sharedData == NULL) {
        return INVALID_PARA_NULL_PTR;
    }

    LOG_INFO("invoked, cmd = 0x%x, dataSize = %u, dataMaxSize = %u", cmd, sharedData->dataSize, sharedData->dataMaxSize);

    uint8_t aid[] = PIN_AUTH_APPLET_AID;
    AppIdentifier identifier = {.aid = aid, .aidLen = sizeof(aid)};
    CardChannel *channel = CreateSecureElementChannel(PIN_AUTH_READER_NAME, &identifier);
    if (channel == NULL) {
        LOG_ERROR("CreateSecureElementChannel error");
        return CHN_OPEN_ERR;
    }

    PinAuthContext context;
    (void)memset_s(&context, sizeof(context), 0, sizeof(context));

    INIT_SE_COMMON_CONTEXT(context.base, sender, channel);

    ResultCode ret = ProcessPinAuthCommandStub(&context, cmd, sharedData);
    DestroySecureElementChannel(channel);
    return ret;
}

SERVICE_REGISTER(SERVICE_ID_PIN_AUTH, PinAuthProcessCommand, PinAuthCheckPermission);
