/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DYN_SERVICE_INC_STORAGE_KEY_DERIVE_H
#define DYN_SERVICE_INC_STORAGE_KEY_DERIVE_H

#include <stdint.h>

#include "se_base_services_defines.h"
#ifdef __cplusplus
extern "C" {
#endif

#define PIN_AUTH_TA_UUID                                   \
    {                                                      \
        0xebeed547, 0xcf6a, 0x4592,                        \
        {                                                  \
            0xb0, 0xf1, 0x2a, 0xe4, 0xa8, 0xcd, 0x43, 0xda \
        }                                                  \
    }

ResultCode StorageKeyDerive(uint32_t sender, uint8_t *key, uint32_t len);

ResultCode SecStorageGetSlotIdByName(uint32_t sender, const char *name, uint8_t *slotId);

#ifdef __cplusplus
}
#endif

#endif // DYN_SERVICE_INC_STORAGE_KEY_DERIVE_H