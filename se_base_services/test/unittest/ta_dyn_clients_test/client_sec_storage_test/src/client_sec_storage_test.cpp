/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <gtest/gtest.h>

#include <vector>

#include "client_sec_storage.h"

namespace OHOS {
namespace SeBaseServices {
namespace UnitTest {
using namespace testing;

TEST(ClientSecStorageTest, SeSecStorageSetFactoryResetAuthenticationKeyCaseNullInput)
{
    uint32_t ret = SeSecStorageSetFactoryResetAuthenticationKey(LEVEL_USER_WIPE, ALGO_NIST_P256, nullptr);
    EXPECT_EQ(ret, INVALID_PARA_NULL_PTR);
}

TEST(ClientSecStorageTest, SeSecStorageSetFactoryResetAuthenticationKeyCaseNullKey)
{
    StorageAuthKey key = {.keyData = nullptr, .keySize = 0};
    uint32_t ret = SeSecStorageSetFactoryResetAuthenticationKey(LEVEL_USER_WIPE, ALGO_NIST_P256, &key);
    EXPECT_NE(ret, SUCCESS);
}

TEST(ClientSecStorageTest, SeSecStorageSetFactoryResetAuthenticationKeyCaseSuccess)
{
    std::vector<uint8_t> vec(32, 0);
    StorageAuthKey key = {.keyData = vec.data(), .keySize = static_cast<uint32_t>(vec.size())};
    uint32_t ret = SeSecStorageSetFactoryResetAuthenticationKey(LEVEL_USER_WIPE, ALGO_NIST_P256, &key);
    EXPECT_EQ(ret, SUCCESS);
}

} // namespace UnitTest
} // namespace SeBaseServices
} // namespace OHOS