# root cmake file

cmake_minimum_required(VERSION 3.16.3)
project(se_base_services)

if(ENABLE_TESTING)
    message(STATUS "building with testing")
    enable_testing()
    include(cmake/enable_testing.cmake)
    add_subdirectory(test)
endif()

if(ENABLE_FACTORY)
    add_compile_definitions(ENABLE_FACTORY=1)
endif()

add_subdirectory(frameworks)
