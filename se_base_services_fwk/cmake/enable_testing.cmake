# enable asan in testing mode

IF(ENABLE_COVERAGE OR ENABLE_FUZZ)
    SET(COVERAGE_FLAG "--coverage")
ENDIF()

set(CMAKE_CXX_FLAGS "-O0 -fno-omit-frame-pointer -fsanitize=address -fexceptions ${COVERAGE_FLAG}")
set(CMAKE_C_FLAGS "-O0 -fno-omit-frame-pointer -fsanitize=address -fexceptions ${COVERAGE_FLAG}")
set(CMAKE_LINKER_FLAG "-fno-omit-frame-pointer -fsanitize=address ${COVERAGE_FLAG}")

add_compile_definitions("ENABLE_TESTING")
