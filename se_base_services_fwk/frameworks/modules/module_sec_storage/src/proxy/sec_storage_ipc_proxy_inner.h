/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CODE_MODULES_INC_STORAGE_IPC_PROXY_INNER_H
#define CODE_MODULES_INC_STORAGE_IPC_PROXY_INNER_H

#include "se_module_sec_storage_defines.h"

#include "sec_storage_ipc_defines.h"

#ifdef __cplusplus
extern "C" {
#endif

ResultCode SetFactoryResetAuthenticationKeyInputToBuffer(FactoryResetLevel level, FactoryResetAuthAlgo algo,
    const StorageAuthKey *key, uint8_t *buffer, uint32_t *size);

ResultCode PrepareFactoryResetOutputFromBuffer(const uint8_t *buffer, uint32_t size, uint8_t *nonce, uint32_t *length);

ResultCode ProcessFactoryResetInputToBuffer(FactoryResetLevel level, const uint8_t *credential, uint32_t length,
    uint8_t *buffer, uint32_t *size);

ResultCode AllocateSlotInputToBuffer(const StorageFileName *name, const StorageSlotAttr *slotAttr,
    const StorageAuthKey *slotKey, uint8_t *buffer, uint32_t *size);

ResultCode GetSlotStatusInputToBuffer(const StorageFileName *name, uint8_t *buffer, uint32_t *size);

ResultCode WriteSlotInputToBuffer(const StorageIndicator *indicator, const StorageDataBuffer *data, uint8_t *buffer,
    uint32_t *size);

ResultCode ReadSlotInputToBuffer(const StorageIndicator *indicator, uint8_t *buffer, uint32_t *size);

ResultCode FreeSlotInputToBuffer(const StorageFileName *name, const StorageAuthKey *key, uint8_t *buffer,
    uint32_t *size);

ResultCode SetAllSlotsSizeInputToBuffer(const uint16_t *slotSizeArray, uint32_t arrayLength, uint8_t *buffer,
    uint32_t *size);

ResultCode GetAllSlotsSizeOutputFromBuffer(const uint8_t *buffer, uint32_t size, uint16_t *slotSizeArray,
    uint32_t *arrayLength);

#ifdef __cplusplus
}
#endif

#endif // CODE_MODULES_INC_STORAGE_IPC_PROXY_INNER_H