/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CODE_MODULES_INC_STORAGE_CORE_INNER_H
#define CODE_MODULES_INC_STORAGE_CORE_INNER_H

#include "response_apdu.h"
#include "se_module_sec_storage_defines.h"
#include "sec_storage_ipc_defines.h"

#ifdef __cplusplus
extern "C" {
#endif

enum {
    INS_STORAGE_SET_RESET_KEY = 0x01,
    INS_STORAGE_GET_RESET_KEY = 0x02,
    INS_STORAGE_PREPARE_RESET = 0x03,
    INS_STORAGE_PROCESS_RESET = 0x04,
    INS_STORAGE_SET_TO_USER_MODE = 0x05,
    INS_STORAGE_GET_AUTH_ALGO = 0x06,
    INS_STORAGE_SET_ALL_SLOT_SIZES = 0x07,
    INS_STORAGE_GET_ALL_SLOT_SIZES = 0x08,
    INS_STORAGE_ALLOC_SLOT = 0x09,
    INS_STORAGE_FREE_SLOT = 0x0a,
    INS_STORAGE_READ_SLOT = 0x0b,
    INS_STORAGE_WRITE_SLOT = 0x0c,
    INS_STORAGE_GET_SLOT_STATUS = 0x0e,
};

enum {
    BIT_VALUE_PK_RESET_ENABLE = (1 << 0),
    BIT_VALUE_SLOT_RESET_ENABLE = (1 << 1),
};

enum {
    BIT_VALUE_FACTORY_WIPE_BIT = (1 << 0),
    BIT_VALUE_AUTH_ON_FREE = (1 << 1),
    BIT_VALUE_AUTH_ON_WRITE = (1 << 2),
    BIT_VALUE_AUTH_ON_READ = (1 << 3),
    BIT_VALUE_ALGO_HMAC_SHA256 = (0 << 4),
    BIT_VALUE_ALGO_HMAC_SM3 = (1 << 4),
    BIT_VALUE_ALGO_RAW = (2 << 4),
};
#define BIT_VALUE_ALGO_MASK (3 << 4)

#define GET_SLOT_STATUS_RSP_APDU_DATA_LEN 16

ResultCode StorageResponseChecker(const ResponseApdu *apdu);

ResultCode StorageGetFactoryResetAuthenticationAlgoFromRspApdu(const ResponseApdu *apdu, FactoryResetAuthAlgo *algo);

ResultCode StoragePrepareFactoryResetFromRspApdu(const ResponseApdu *apdu, uint8_t *nonce, uint32_t *length);

ResultCode StorageGetSupportedAlgorithmsFromRspApdu(const ResponseApdu *apdu, uint8_t *resetAlgo, uint8_t *operAlgo);

ResultCode StorageGetAllSlotsSizeFromRspApdu(const ResponseApdu *apdu, uint16_t *slotSizeArray, uint32_t *arrayLength);

ResultCode StorageGetSlotStatusFromRspApdu(const ResponseApdu *apdu, StorageSlotStatus *status);

ResultCode ConvertStorageSlotAttrToBitMap(const StorageSlotAttr *attr, uint8_t *flag);

ResultCode StorageWriteSlotToCmdApduData(const StorageAuthKey *key, const StorageDataArea *area,
    const StorageDataBuffer *data, uint8_t *body, uint32_t *size);

ResultCode StorageReadSlotToCmdApduData(const StorageAuthKey *key, const StorageDataArea *area, uint8_t *body,
    uint32_t *size);

ResultCode StorageReadSlotFromRspApdu(const ResponseApdu *apdu, StorageDataBuffer *data);

ResultCode StorageGetSlotStatusParseFlag(uint8_t flag, StorageSlotAttr *attr);

#ifdef __cplusplus
}
#endif

#endif // CODE_MODULES_INC_STORAGE_CORE_INNER_H