/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CODE_MODULES_INC_STORAGE_STUB_INNER_H
#define CODE_MODULES_INC_STORAGE_STUB_INNER_H

#include "se_base_services_defines.h"
#include "sec_storage_ipc_defines.h"
#include "sec_storage_ipc_stub.h"

#ifdef __cplusplus
extern "C" {
#endif

ResultCode ProcStorageCmdSetFactoryResetAuthKeyStub(SecStorageContext *context, SharedDataBuffer *buffer);

ResultCode ProcStorageCmdGetFactoryResetAuthKeyAlgoStub(SecStorageContext *context, SharedDataBuffer *buffer);

ResultCode ProcStorageCmdPrepareFactoryResetStub(SecStorageContext *context, SharedDataBuffer *buffer);

ResultCode ProcStorageCmdProcessFactoryResetStub(SecStorageContext *context, SharedDataBuffer *buffer);

ResultCode ProcStorageCmdSetToUserModeStub(SecStorageContext *context, SharedDataBuffer *buffer);

ResultCode ProcStorageCmdGetSlotOperateAlgorithmStub(SecStorageContext *context, SharedDataBuffer *buffer);

ResultCode ProcStorageCmdGetFactoryResetAlgorithmStub(SecStorageContext *context, SharedDataBuffer *buffer);

ResultCode ProcStorageCmdSetAllSlotsSizeStub(SecStorageContext *context, SharedDataBuffer *buffer);

ResultCode ProcStorageCmdGetAllSlotsSizeStub(SecStorageContext *context, SharedDataBuffer *buffer);

ResultCode ProcStorageCmdAllocateSlotStub(SecStorageContext *context, SharedDataBuffer *buffer);

ResultCode ProcStorageCmdWriteSlotStub(SecStorageContext *context, SharedDataBuffer *buffer);

ResultCode ProcStorageCmdReadSlotStub(SecStorageContext *context, SharedDataBuffer *buffer);

ResultCode ProcStorageCmdFreeSlotStub(SecStorageContext *context, SharedDataBuffer *buffer);

ResultCode ProcStorageCmdGetSlotStatusStub(SecStorageContext *context, SharedDataBuffer *buffer);

ResultCode ProcessStorageKeyDerive(SecStorageContext *context, uint8_t *key, uint32_t len);

ResultCode ConvertStorageFileNameToId(const SecStorageContext *context, StorageFileName *name, uint8_t *slotId);

ResultCode EnableStorageScpProtocol(const SecStorageContext *context);

#ifdef __cplusplus
}
#endif

#endif // CODE_MODULES_INC_STORAGE_STUB_INNER_H