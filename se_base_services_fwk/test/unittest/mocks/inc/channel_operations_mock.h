/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef UNIT_TEST_INC_CHANNEL_OPERATIONS_MOCKER_H
#define UNIT_TEST_INC_CHANNEL_OPERATIONS_MOCKER_H

#ifndef ENABLE_TESTING
#error only in test mode
#endif
#include "card_channel.h"

#include <gmock/gmock.h>

#include "function_mocker.h"

typedef struct SecureElementContext {
    uint32_t *handle;
} SecureElementContext;

namespace OHOS {
namespace SeBaseServices {
namespace UnitTest {

class ChannelOperationsMock : public FunctionMocker<ChannelOperationsMock> {
public:
    static ResultCode MockChannelTransmit(SecureElementContext *context, const uint8_t *command, uint32_t commandLen,
        uint8_t *response, uint32_t *responseLen);

    static ResultCode MockChannelOpen(SecureElementContext *context);

    static ResultCode MockChannelClose(SecureElementContext *context);

    static ResultCode MockChannelGetOpenResponse(SecureElementContext *context, uint8_t *response,
        uint32_t *responseLen);

    MOCK_METHOD(ResultCode, ChannelTransmit,
        (SecureElementContext * context, const uint8_t *command, uint32_t commandLen, uint8_t *response,
            uint32_t *responseLen));
    MOCK_METHOD(ResultCode, ChannelOpen, (SecureElementContext * context));
    MOCK_METHOD(ResultCode, ChannelClose, (SecureElementContext * context));
    MOCK_METHOD(ResultCode, ChannelGetOpenResponse,
        (SecureElementContext * context, uint8_t *response, uint32_t *responseLen));
};

} // namespace UnitTest
} // namespace SeBaseServices
} // namespace OHOS
#endif // UNIT_TEST_INC_CHANNEL_OPERATIONS_MOCKER_H
